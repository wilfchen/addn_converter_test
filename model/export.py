import numpy as np
import mindspore.context as context
from mindspore import Tensor, export
from mindspore.ops import operations as P
import mindspore.nn as nn

context.set_context(mode=context.GRAPH_MODE, device_target='GPU', save_graphs=False)

class AddN(nn.Cell):
    def __init__(self):
        super(AddN, self).__init__()
        self.add_n = P.AddN()

    def construct(self, x1, x2, x3):
        return self.add_n((x1, x2, x3))

x1 = Tensor(np.arange(1 * 3 * 32 * 32).reshape(1, 3, 32, 32).astype(np.float32))
x2 = Tensor(np.arange(1 * 3 * 32 * 32).reshape(1, 3, 32, 32).astype(np.float32))
x3 = Tensor(np.arange(1 * 3 * 32 * 32).reshape(1, 3, 32, 32).astype(np.float32))

net = AddN()
export(net, x1, x2, x3, file_name='../addn/1/addn', file_format='MINDIR')
