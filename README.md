# AddN转换函数开发

## MindSpore软件包编译

- 下载并解压TensorRT
  ```
  # https://developer.nvidia.com/compute/machine-learning/tensorrt/secure/7.2.3/tars/TensorRT-7.2.3.4.Ubuntu-16.04.x86_64-gnu.cuda-11.1.cudnn8.1.tar.gz
  tar -vzxf TensorRT-7.2.3.4.Ubuntu-16.04.x86_64-gnu.cuda-11.1.cudnn8.1.tar.gz
  export LD_LIBRARY_PATH=${TENSORRT_HOME}/lib/:$LD_LIBRARY_PATH
  ```

- 克隆MindSpore代码仓
  ```
  git clone https://gitee.com/mindspore/mindspore.git
  ```

- 编译MindSpore安装包
  ```
  bash build.sh -e gpu -j 8 -L ${TENSORRT_HOME} -V 11.1
  pip install build/package/mindspore_gpu-xxxxx.whl
  ```

- 编译Serving安装包
  ```
  git clone https://gitee.com/mindspore/serving.git
  bash build.sh -e gpu -p ${MindSpore}/lib
  pip install build/package/mindspore_serving-xxxx.whl
  ```

## 测试用例执行
- 克隆用例
  ```
  git clone https://gitee.com/wilfchen/addn_converter_test.git
  cd addn_converter_test
  ```

- 导出MindIR模型
  ```
  cd model
  python export.py
  cd ..
  ls 1/addn.mindir
  ```

- 部署推理服务
  ```
  export GLOG_v=2
  python master_with_worker.py
  ```

  可以看到，目前暂不支持使用TensorRT库执行AddN操作
  ```text
  [WARNING] PRE_ACT(219099, 7fa39e670700, python): 2021-06-28-19:03:17.114.093[mindspore/ccsrc/backend/optimizer/trt_pass/trt_op_factory.h:54 GetConvertFunc] Operator: AddN not support.
  ```

- 发起推理请求

  再重新打开一个shell终端
  ```
  python client.py
  ```
