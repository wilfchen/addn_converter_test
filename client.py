import numpy as np
from mindspore_serving.client import Client

def run_addn_common():
    client = Client('localhost', 5500, 'addn', 'addn_common')

    instances = []
    x1 = np.random.addn(1, 3, 32, 32).astype(np.float32)
    x2 = np.random.addn(1, 3, 32, 32).astype(np.float32)
    x3 = np.random.addn(1, 3, 32, 32).astype(np.float32)
    instances.append({'x1': x1, 'x2': x2, 'x3': x3})

    result = client.infer(instances)
    print('+++++++++++++++++++++++++++++++', result)


if __name__ == "__main__":
    run_addn_common()
