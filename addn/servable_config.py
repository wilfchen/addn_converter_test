from mindspore_serving.worker import register

register.declare_servable(servable_file='addn.mindir', model_format='mindir', with_batch_dim=False)

@register.register_method(output_names=['y'])
def addn_common(x1, x2, x3):
    y = register.call_servable(x1, x2, x3)
    return y
