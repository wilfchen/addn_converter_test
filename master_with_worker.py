import os
from mindspore_serving import master
from mindspore_serving import worker

def start():
    servable_dir = os.path.abspath('.')
    worker.start_servable_in_master(servable_dir, 'addn', device_type='GPU', device_id=0)
    master.start_grpc_server('127.0.0.1', 5500)

if __name__ == "__main__":
    start()
